const detalhesJogador = () => {

    let details = document.querySelector(".details");
    let jogador = document.cookie.split("*");

    details.innerHTML = `
        <div class="container">
            <img class="imagem" src="${jogador[2]}">
            <h2 class="nome">${jogador[0]}</h2>
            <div class="detalhes">
                <p class="nome_completo">${jogador[4]}</p>
                <p class="ano_nasc">Nascido em ${jogador[5]}</p>
                <p class="alt_peso">Altura: ${jogador[6]}</p>
            </div>
            <p class="historia">${jogador[3]}</p>
            <h3 class="posicao">${jogador[1]}</h3>
        </div>
        `;
}

window.onload = () => {
    detalhesJogador()

    const backHome = document.querySelector(".btn_voltar");

    if (backHome) {
        backHome.addEventListener("click", () => {
            window.location.href = "index.html"
        })
    }
}
