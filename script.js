const criaCards = (jogadores) => {
    let container = document.querySelector("#container");

    for (let jogador = 0; jogador <= jogadores.length; jogador++) {
        container.innerHTML +=`
            <div class="card" onclick='coletaJogador(${JSON.stringify(jogadores[jogador])})'>
                <img src="${jogadores[jogador]["imagem"]}" class="img"/>
                <h3 class="jogador">${jogadores[jogador]["nome"]}</h3>
                <p class="in_posicao">${jogadores[jogador]["posicao"]}</p>
            </div>
        `;
    }
};

const buscaJogador = () => {
    const input = document.getElementById("search").value.toUpperCase();
    const cards = document.getElementsByClassName("card");

    for (let i = 0; i <= cards.length; i++) {
        let nome = cards[i].querySelector(".card h3");
        if (nome.innerText.toUpperCase().indexOf(input) > -1) {
            cards[i].style.display = "";
        } else {
            cards[i].style.display = "none";
        }
    }
};

const coletaJogador = (jogador) => {
    document.cookie =  jogador["nome"] + "*"
    document.cookie += jogador["posicao"] + "*"
    document.cookie += jogador["imagem"] + "*"
    document.cookie += jogador["descricao"] + "*"
    document.cookie += jogador["nome_completo"] + "*"
    document.cookie += jogador["nascimento"] + "*"
    document.cookie += jogador["altura_peso"]

    window.location.href = "details.html"
    
    document.cookie = ""
};

window.onload = () => {
    criaCards(jogadores)
}
